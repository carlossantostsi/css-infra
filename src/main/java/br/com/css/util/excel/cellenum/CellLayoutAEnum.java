package br.com.css.util.excel.cellenum;

public enum CellLayoutAEnum {

	NAME(1), LASTNAME(2);

	private int index;

	private CellLayoutAEnum(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

}
