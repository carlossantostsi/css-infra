package br.com.css.util.excel;

import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public abstract class ExcelReader<T> extends ExcelSupport {

	private Workbook workbook;
	private boolean containsHeader;

	private List<T> registros;

	public ExcelReader(String path, boolean containsHeader) throws IOException {
		this.containsHeader = containsHeader;
		this.workbook = readWorkbook(path);

		convertRows(this.readSheet(workbook, 0));
		this.workbook.close();
	}

	protected void convertRows(Sheet sheet) {
		for (Row row : sheet) {
			if (this.containsHeader && row.getRowNum() == 0) {
				continue;
			}
			registros.add(covertRow(row));
		}
	}

	protected abstract T covertRow(Row row);

	public List<T> getRegistros() {
		return registros;
	}

}
