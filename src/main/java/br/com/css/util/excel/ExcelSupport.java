package br.com.css.util.excel;

import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelSupport {

	// Read workbook String path
	protected Workbook readWorkbook(String path) throws IOException {
		return new XSSFWorkbook(path);
	}

	// Read Sheet
	protected Sheet readSheet(Workbook workbook, int index) {
		return workbook.getSheetAt(0);
	}

	protected Sheet readSheet(Workbook workbook, String name) {
		return workbook.getSheetAt(0);
	}

}
