package br.com.css;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CssProjectsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CssProjectsApplication.class, args);
	}

}
